﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenuUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void _Accept() {
		SceneManager.LoadScene ("Level_1_Chandler");
	}

	public void _Deny() {
		Application.Quit ();
	}
	public void _Credits() {
		SceneManager.LoadScene ("Credits");
	}
}
